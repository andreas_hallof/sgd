#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import glob, base64, cbor, time

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes

print("Lade Zertifikate und private Signaturschlüssel")

users = []
for cert_filename in sorted(glob.glob("pki/*.der")):
    with open(cert_filename, "rb") as f:
        certdata = base64.b64encode(f.read())
    key_filename = (cert_filename[:-3] + "pem").replace("cert", "key")
    with open(key_filename, "rb") as f:
        signing_key = serialization.load_pem_private_key(f.read(),
                      password=None, backend=default_backend())
    users.append([certdata, signing_key])
        
print("Ich habe", len(users), "Nutzer.")

H_SGD1 = "39ee5f1765915640809456c22ecfcb070e3469c7f79eb4341bf14b407243f994"
H_SGD2 = "af191ec5cd8247bc08a1b9618b38117294f5d0fd96c437e1d7bc6fd7012463db"

#
# Ich lasse jeden Nutzer nacheinander einmal das SGD-Protokoll durchlaufen
# D. h. die RVE muss je zwei mal pro Nutzer die Signatur prüfen
#
print("start 0% ktr simulation")
start = time.time()
res = []
for current_user in users:
    cu_private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
    cu_public_key  = cu_private_key.public_key()
    cu_pn          = cu_public_key.public_numbers()
    cu_public_key_ecies = " ".join([cu_pn.curve.name, hex(cu_pn.x), hex(cu_pn.y), H_SGD1, H_SGD2]).encode()
    cu_signature = current_user[1].sign(cu_public_key_ecies, ec.ECDSA(hashes.SHA256()))
    a = [cu_public_key_ecies, cu_signature, current_user[0]]
    res.extend([a, a])

print("ende 0% ktr simulation {:.2f} sec, len={}".format(time.time()-start, len(res)))

with open("sim-0-p.cbor", "wb") as f:
    f.write(cbor.dumps(res))

ktr_private_key = users[-1][1]
ktr_public_key  = ktr_private_key.public_key()
ktr_pn          = ktr_public_key.public_numbers()
ktr_public_key_ecies = " ".join([ktr_pn.curve.name, hex(ktr_pn.x), hex(ktr_pn.y), H_SGD1, H_SGD2]).encode()
ktr_signature = ktr_private_key.sign(ktr_public_key_ecies, ec.ECDSA(hashes.SHA256()))

#
# Jetzt lasse ich 90% der Nutzer das SGD-Protokoll durchlaufen
# und die letzten 10% der Protokolldurchläufe kommen von genau einem Nutzer
# (bspw. einen KTR-Consumer)
#
print("start 10% ktr simulation")
start = time.time()
ninty_percent = int(len(users)*0.9)
i = 0
res = []
for current_user in users:
    if i > ninty_percent:
        current_user = users[-1]
        signature = ktr_signature
        public_key_ecies = ktr_public_key_ecies
    else:
        private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
        public_key  = private_key.public_key()
        pn          = public_key.public_numbers()
        public_key_ecies = " ".join([pn.curve.name, hex(pn.x), hex(pn.y), H_SGD1, H_SGD2]).encode()
        signature = current_user[1].sign(public_key_ecies, ec.ECDSA(hashes.SHA256()))

    a = [public_key_ecies, signature, current_user[0]]
    res.extend([a, a])
    i += 1

print("ende 10% ktr simulation {:.2f} sec, len={}".format(time.time()-start, len(res)))

with open("sim-10-p.cbor", "wb") as f:
    f.write(cbor.dumps(res))

#
# Jetzt lasse ich 10% der Nutzer das SGD-Protokoll durchlaufen
# und die letzten 90% SGD-Protokolldurchläufe kommen von genau einem Nutzer
# (bspw. einen KTR-Consumer)
#
print("start 90% ktr simulation")
start = time.time()
ten_percent = int(len(users)*0.1)
i = 0
res = []
for current_user in users:
    if i > ten_percent:
        current_user = users[-1]
        signature = ktr_signature
        public_key_ecies = ktr_public_key_ecies
    else:
        private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
        public_key  = private_key.public_key()
        pn          = public_key.public_numbers()
        public_key_ecies = " ".join([pn.curve.name, hex(pn.x), hex(pn.y), H_SGD1, H_SGD2]).encode()
        signature = current_user[1].sign(public_key_ecies, ec.ECDSA(hashes.SHA256()))

    a = [public_key_ecies, signature, current_user[0]]
    res.extend([a, a])
    i += 1

print("ende 90% ktr simulation {:.2f} sec, len={}".format(time.time()-start, len(res)))

with open("sim-90-p.cbor", "wb") as f:
    f.write(cbor.dumps(res))

