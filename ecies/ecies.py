#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, attr, typing, re, pyfiglet, shutil, secrets
from base64 import b64encode, b64decode
from binascii import hexlify, unhexlify
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives import serialization
#from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat


def is_on_curve(x : int, y : int):
    #E: y^2 = x^3 + A*x + B mod p
    p = 0xA9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377
    A = 0x7D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9
    B = 0x26DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6

    linke_seite=pow(y,2,p)
    rechte_seite=(pow(x,3,p) + A*x + B) % p

    return linke_seite == rechte_seite

def ConvertPublicKey(S_RPK : str):

    tmp=re.split(r'\s+', S_RPK)
    assert len(tmp) in [3, 5]
    x=int(tmp[1],0)
    y=int(tmp[2],0)
    assert is_on_curve(x,y)

    ## leider unterstützt aktuell das openssl-backend den "direkten" Import (PublicNumbers) nicht
    ##  File "/usr/lib/python3.7/site-packages/cryptography/hazmat/primitives/asymmetric/ec.py", line 359, in public_key
    ## return backend.load_elliptic_curve_public_numbers(self)
    ## AttributeError: 'function' object has no attribute 'load_elliptic_curve_public_numbers'

    #RecipientPN=ec.EllipticCurvePublicNumbers(x, y, ec.BrainpoolP256R1())
    ## deswegen einen Umweg nehmen

    #t1=ephemeral_public_key.public_bytes(Encoding.DER, PublicFormat.SubjectPublicKeyInfo)
    #print(hexlify(t1))
    p_x='{:x}'.format(x)
    while len(p_x)<64:
        p_x="0"+p_x
    #print("p_x", len(p_x))
    p_y='{:x}'.format(y)
    while len(p_y)<64:
        p_y="0"+p_y
    #print("p_y", len(p_y))
    t1='305a301406072a8648ce3d020106092b240303020801010703420004' + p_x + p_y
    t2=unhexlify(t1)

    RPK=serialization.load_der_public_key(t2, backend=default_backend())
    PN_RPK=RPK.public_numbers()
    assert PN_RPK.x == x
    assert PN_RPK.y == y

    return RPK

def EncryptECIES(RecipientPublicKey : str, Plaintext : str):
    """
        Erzeugt eine ECIES-verschlüsselte Nachricht im Format nach A_17902
        (Beispiel:
            brainpoolP256r1 0x2ea3a5bc99911bf967d59df494455887b2bd7d56bb8d87dcfc938644e29ab514 0x5bc0b86eeab7a1cadaa31050283f304a3348d98525ba2002a9a918b604a7329a 0x941c93f67d9a6194112ec7369a0dba69888bf891ee9669593d8b92b7bdf7f339 0x79825ccb48dd073aab0814c299cb5e7d4f30f803fcef520dad73ade6a5e1ffe9 A4JpEBnDnyQ8IojzZf3cFcBeJDeho2x7r4+wp4mShoAznclH0i3vY1Yi8jya5wIptVklFMhgcDflYnAq3huw1KFHGZ3rTptbABnnKdeT77twU68AnFUAOK5IN/gtixDPzHgv9i/A
        )

        Eingabe:
            RecipientPublic im Format nach A_17894 also "<KurvenID> 0x<X-Koodinate> 0x<Y-Koodinate>"
            Plaintext -- wird nicht ausgewertet / wird als opakes Zeichenkette verwendet

    """
    assert RecipientPublicKey.startswith("brainpoolP256r1 ")
    rpk_ar=re.split(r'\s+', RecipientPublicKey)
    print(len(rpk_ar), rpk_ar)
    if len(rpk_ar)!=3:
        if len(rpk_ar)==5:
            rpk=" ".join(rpk_ar[0:3])
        else:
            raise ValueError("RecipeientPublicKey wrong format")
    else:
        rpk=RecipientPublicKey

    res=rpk + " "
    ephemeral_private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
    ephemeral_public_key  = ephemeral_private_key.public_key()
    ephemeral_pn          = ephemeral_public_key.public_numbers()
    ephemeral_public_key_string = "{} {} ".format(hex(ephemeral_pn.x), hex(ephemeral_pn.y))
    res+=ephemeral_public_key_string

    RPK=ConvertPublicKey(RecipientPublicKey)

    shared_ecdh_secret=ephemeral_private_key.exchange( ec.ECDH(), RPK )
    #print(len(shared_ecdh_secret))
    aes_key = HKDF(
                algorithm=hashes.SHA256(),
                length=32,
                salt=None,
                info=b'', backend=default_backend()
              ).derive(shared_ecdh_secret)
    
    #print("Encrypt aes-key:", hexlify(aes_key))

    iv=os.urandom(12)
    #print("Encrypt IV:", hexlify(iv))
    aesgcm=AESGCM(aes_key)
    Ciphertext=aesgcm.encrypt(iv, Plaintext.encode(), None)
    B_Ciphertext=b64encode(iv+Ciphertext)

    return res + B_Ciphertext.decode()

def DecryptECIES(PrivateKey : ec.EllipticCurvePrivateKey, Ciphertext_A_17902 : str):
    """
        Entschlüsselt ein Chiffrat im Format nach A_17902

        Eingabe:
            PrivateKey -- privater ECC-Schlüssel (Skalar)
            Ciphertext muss im Format nach A_17902 sein.
            (Beispiel:
                brainpoolP256r1 0x2ea3a5bc99911bf967d59df494455887b2bd7d56bb8d87dcfc938644e29ab514 0x5bc0b86eeab7a1cadaa31050283f304a3348d98525ba2002a9a918b604a7329a 0x941c93f67d9a6194112ec7369a0dba69888bf891ee9669593d8b92b7bdf7f339 0x79825ccb48dd073aab0814c299cb5e7d4f30f803fcef520dad73ade6a5e1ffe9 A4JpEBnDnyQ8IojzZf3cFcBeJDeho2x7r4+wp4mShoAznclH0i3vY1Yi8jya5wIptVklFMhgcDflYnAq3huw1KFHGZ3rTptbABnnKdeT77twU68AnFUAOK5IN/gtixDPzHgv9i/A
            )

        Ausgabe:
            einen String (KLartext) (kein bytearray)

    """

    PN  = PrivateKey.public_key().public_numbers()
    PublicKeyString = "{} {} {} ".format(PN.curve.name, hex(PN.x), hex(PN.y))

    if not Ciphertext_A_17902.startswith(PublicKeyString): 
        raise ValueError("Privater Schlüssel passt nicht zum öffentlichen Empfängerschlüssel")

    ct=re.split(r'\s+', Ciphertext_A_17902)
    assert len(ct)==6
    assert len(ct[5])>12+16 # 12 byte iv, 16 ist geraten (untere Schrank die nach SGD-Protokoll sowieso immer überschritten ist)

    S_E_PK="{} {} {}".format(ct[0], ct[3], ct[4])

    RPK=ConvertPublicKey(S_E_PK)

    shared_ecdh_secret=PrivateKey.exchange( ec.ECDH(), RPK )
    #print(len(shared_ecdh_secret))
    aes_key = HKDF(
                algorithm=hashes.SHA256(),
                length=32,
                salt=None,
                info=b'', backend=default_backend()
              ).derive(shared_ecdh_secret)
    
    #print("Decrypt aes-key:", hexlify(aes_key))

    Ciphertext=b64decode(ct[5])
    iv=Ciphertext[:12]
    assert len(iv)==12
    #print("Decrypt IV:", hexlify(iv))
    Ciphertext_pure=Ciphertext[12:]

    aesgcm=AESGCM(aes_key)
    pt=aesgcm.decrypt(iv, Ciphertext_pure, None)

    return pt.decode()

def banner(s):
    if not s:
        return
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    ## https://www.programcreek.com/python/example/96392/pyfiglet.Figlet
    #print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))
    # slant, basic, gothic, small, cybermedium
    print(pyfiglet.Figlet(font='slant', width=int(x.columns)).renderText(s))

if __name__=='__main__':

    banner("Schritt 0")
    print("Empfänger Schlüssel erzeugen (Das wäre dann bspw. der erhaltene SGD-HSM-Schlüssel)")

    private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
    public_key  = private_key.public_key()
    pn          = public_key.public_numbers()
    public_key_string = "{} {} {}".format(pn.curve.name, hex(pn.x), hex(pn.y))
    #t=pn.public_key(default_backend())

    #tmp=public_key.public_bytes(Encoding.DER, PublicFormat.SubjectPublicKeyInfo)
    #print("Start", hexlify(tmp))
    #tmp2=serialization.load_der_public_key(tmp, backend=default_backend())

    print(public_key_string)

    banner("1")
    print("Verschlüssele Nachricht")
    message="Challenge {}".format(secrets.token_hex(32))
    ciphertext=EncryptECIES(public_key_string, message)
    print("Ciphertext:\n", ciphertext)

    banner("2")
    print("Entschlüssele Nachricht")
    plaintext=DecryptECIES(private_key, ciphertext)
    print("Plaintext:\n", plaintext)

    


