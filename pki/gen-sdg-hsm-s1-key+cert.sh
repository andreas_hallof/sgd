#! /usr/bin/env bash

openssl ecparam -name brainpoolP256r1 -genkey -out sgd_hsm_ecies_key_1.pem
openssl ecparam -name brainpoolP256r1 -genkey -out sgd_hsm_ecies_key_2.pem
#openssl ecparam -name prime256v1 -genkey -out key_$i.pem
#strace openssl req -x509 -key key_$i.pem -out cert_$i.pem -days 365 -subj "/C=DE/ST=Berlin/L=Berlin/O=gemaik/OU=PoC/CN=TSL-Signaturbestaetiger $i"

openssl req -x509 -key sgd_hsm_ecies_key_1.pem \
-outform der \
-out sgd_hsm_ecies_cert_1.der -days 365 \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=SGD-HSM SGD 1"

openssl req -x509 -key sgd_hsm_ecies_key_2.pem \
-outform der \
-out sgd_hsm_ecies_cert_2.der -days 365 \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=SGD-HSM SGD 1"

