#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, json, sys, time
import matplotlib.pyplot as plt

logfile_name = "ibm-sgd1.log" if len(sys.argv) == 1 else sys.argv[1]

p = 0xA9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377
p1= 0xa9

x_werte = []
y_werte = []

with open(logfile_name, "rt") as mylogfile:
    while line := mylogfile.readline():
        entry = json.loads(line)
        if 'pub_key' in entry:
            (x_str, y_str) = entry["pub_key"].split()
            x_str = x_str[0:4]
            y_str = y_str[0:4]
            x_werte.append(int(x_str, 16))
            y_werte.append(int(y_str, 16))

plt.plot(x_werte, y_werte, 'o', color='blue')
plt.savefig(os.path.basename(logfile_name)+"-heatmap.png")

