#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import json, sys, rrdtool

rrd_name = "ibm-sgd1.rrd" if len(sys.argv) == 1 else sys.argv[1]

line_counter = 0
while line := sys.stdin.readline():
    line_counter += 1
    #print("line", line_counter)
    mydata = json.loads(line)
    rrdtool.update(rrd_name, 
                "{}:{}".format(
                            mydata["time_probe"],
                            int(mydata["time"] * 1000)
                            )
                )
