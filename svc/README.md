# Signature Verification Caching (SVC)

## 100.000 Identitäten

1) in pki mittel `gen.sh 100000` Test-Identitäten erzeugen

    ...
    9994 99995 99996 99997 99998 99999 100000
    benötigte Zeit um 100000 Schlüssel + Zertifikat zu erzeugen: 1679 Sekunden

    [a@h pki]$ pwd
    /home/a/git/sgd/svc/pki
    [a@h pki]$ cd ..
    [a@h svc]$ ls pki | grep .der | wc -l
    100000

2) die Test-Datensätze erzeugen `gen-simulations.py`

    [a@h svc]$ ./gen-simulations.py
    Lade Zertifikate und private Signaturschlüssel
    Ich habe 100000 Nutzer.
    start 0% ktr simulation
    ende 0% ktr simulation 225.51 sec, len=200000
    start 10% ktr simulation
    ende 10% ktr simulation 203.04 sec, len=200000
    start 90% ktr simulation
    ende 90% ktr simulation 22.82 sec, len=200000

(nicht wundern: 
ohne Argumente gibt `du` die Datengröße in KiB an:)

    [a@h svc]$ du *.cbor
    219116  sim-0-p.cbor
    219120  sim-10-p.cbor
    219140  sim-90-p.cbo

    [a@h svc]$ du -b *.cbor
    224372753	sim-0-p.cbor
    224375071	sim-10-p.cbor
    224396971	sim-90-p.cbor

3) die Simulation mit `run_sim.py` starten

    [a@h svc]$ du *.cbor
    219116  sim-0-p.cbor
    219120  sim-10-p.cbor
    219140  sim-90-p.cbor
    [a@h svc]$ ./run_sim.py
    Simulation sim-0-p.cbor ohne SVC 222.17711806297302
    Simulation sim-0-p.cbor mit SVC 114.09727025032043
    Ratio 1.947260592436032
    aktueller Speicherverbrauch Cache 4194520 Bytes
    Simulation sim-10-p.cbor ohne SVC 221.50998449325562
    Simulation sim-10-p.cbor mit SVC 102.86788630485535
    Ratio 2.1533443764635845
    aktueller Speicherverbrauch Cache 4194520 Bytes
    Simulation sim-90-p.cbor ohne SVC 221.39837312698364
    Simulation sim-90-p.cbor mit SVC 12.929264068603516
    Ratio 17.123818645224468
    aktueller Speicherverbrauch Cache 524504 Bytes


## 1000 Identitäten

1) in pki mittel `gen.sh` Test-Identitäten erzeugen

1000 Identität sind der default.

2) die Test-Datensätze erzeugen `gen-simulations.py`

Beispiel-Durchläufe:

    (alter Linux PC)

    [a@h svc]$ ./gen-simulations.py 
    Lade Zertifikate und private Signaturschlüssel
    Ich habe 1000 Nutzer.
    start 0% ktr simulation
    ende 0% ktr simulation 2.26 sec, len=1000
    start 10% ktr simulation
    ende 10% ktr simulation 2.13 sec, len=1000
    start 90% ktr simulation
    ende 90% ktr simulation 1.20 sec, len=1000

    (Linux VM)

    a@t:~/git/sgd/svc$ ./gen-simulations.py
    Lade Zertifikate und private Signaturschlüssel
    Ich habe 1000 Nutzer.
    start 0% ktr simulation
    ende 0% ktr simulation 1.51 sec, len=1000
    start 10% ktr simulation
    ende 10% ktr simulation 1.39 sec, len=1000
    start 90% ktr simulation
    ende 90% ktr simulation 0.76 sec, len=1000
    

3) die Simulation mit `run_sim.py` starten

    (Linux VM)

    a@t:~/git/sgd/svc$ ./run_sim.py
    Simulation sim-0-p.cbor ohne SVC 1.4136183261871338
    Simulation sim-0-p.cbor mit SVC 0.7638671398162842
    Simulation sim-10-p.cbor ohne SVC 1.466214656829834
    Simulation sim-10-p.cbor mit SVC 0.6976752281188965
    Simulation sim-90-p.cbor ohne SVC 1.468184471130371
    Simulation sim-90-p.cbor mit SVC 0.09216737747192383

   => Simualation  0p 1.85
   => Simualation 10p 2.04
   => Simualation 90p 15.66

## 10.000 Identitäten

das mal mit 10.000 Identitäten

    [a@h svc]$ du -sh sim-0-p.cbor
    22M     sim-0-p.cbor
    [a@h svc]$ ./run_sim.py
    Simulation sim-0-p.cbor ohne SVC 22.21495771408081
    Simulation sim-0-p.cbor mit SVC 11.420240640640259
    Ratio 1.9452267612493463
    Simulation sim-10-p.cbor ohne SVC 22.180510997772217
    Simulation sim-10-p.cbor mit SVC 10.284668207168579
    Ratio 2.1566579058245208
    Simulation sim-90-p.cbor ohne SVC 22.311437606811523
    Simulation sim-90-p.cbor mit SVC 1.296149492263794
    Ratio 17.213629862897537

