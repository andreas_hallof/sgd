#! /bin/bash

# rrdtool create rrd.ibm-sdg1 --start 1615585807 \

echo "$1"

if [ "$1" = "../ibm-sgd1.log" ]; then
	rrdtool create ibm-sgd1.rrd --start 1615584907 \
		--step 900 \
		DS:tta:GAUGE:2000:0:20000 \
		RRA:AVERAGE:0.5:1:1200
else
	rrdtool create sgd2-via-ibm.rrd --start 1615584907 \
		--step 900 \
		DS:tta:GAUGE:2000:0:20000 \
		RRA:AVERAGE:0.5:1:1200
fi

