#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, attr, typing, re, pyfiglet, shutil, secrets, requests, json
from base64 import b64encode, b64decode
from binascii import hexlify, unhexlify

import requests

url = 'http://maps.googleapis.com/maps/api/directions/json'

params = dict(
    origin='Chicago,IL',
    destination='Los+Angeles,CA',
    waypoints='Joplin,MO|Oklahoma+City,OK',
    sensor='false'
)

resp = requests.get(url=url, params=params)
data = resp.json() # Check the JSON Response Content documentation below

print(type(data))
print(data)

print("Response:", resp.text)
print("Headers:" , resp.headers)
print("HTTP-Status:", resp.status_code)

