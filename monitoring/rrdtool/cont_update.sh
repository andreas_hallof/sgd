#! /bin/bash

set -u

sleep_interval=$((60*15))

while [ ! -f STOP ]; do
	date
	make
	echo waiting $sleep_interval seconds
	sleep $sleep_interval 
done

