#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys, base64, json, hashlib

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import hashes
from cryptography.exceptions import InvalidSignature


with open("user-request.json","rt") as f:
    req = json.loads(f.read())

user_cert_der = base64.b64decode(req['Certificate'])
user_cert = x509.load_der_x509_certificate(user_cert_der, default_backend())

if not isinstance(user_cert.public_key(), rsa.RSAPublicKey):
    sys.exit("Kein RSA-Zertifikate (Nutzer-Schlüssel ist kein RSA-Schlüssel)")

try:
    user_cert.public_key().verify( 
            base64.b64decode(req['Signature']),
            req['PublicKeyECIES'].encode(),
            padding.PSS(mgf=padding.MGF1(hashes.SHA256()), salt_length=32),
            hashes.SHA256() 
    )
except InvalidSignature:
    sys.exit("FAIL: Signaturprüfung gibt ein FAIL zurück")
else:
    print("Signatur OK")

#
# So aus reichem Spass an der Freude verifiziere ich die Signatur mal
# "von Hand".
#

try:
    hash_dtbs = hashlib.sha256(req['PublicKeyECIES'].encode()).digest()
    x = user_cert.public_key().public_numbers()
    s = int.from_bytes(base64.b64decode(req['Signature']), byteorder='big')
    m = pow(s, x.e, x.n)
    mb = m.to_bytes((m.bit_length() + 7) // 8, 'big')

    #4.   If the rightmost octet of EM does not have hexadecimal value
    #     0xbc, output "inconsistent" and stop.

    if mb[-1] != 0xbc:
        raise InvalidSignature

    #5.   Let maskedDB be the leftmost emLen - hLen - 1 octets of EM,
    #     and let H be the next hLen octets.

    maskedDB = mb[:-(32 + 1)]
    H = mb[-33:-1]

    #7.   Let dbMask = MGF(H, emLen - hLen - 1).

    dbMask = b''; counter = 0
    while len(dbMask) < len(maskedDB):
        tmp_hash = hashlib.sha256(H + b'\0\0\0' + bytes([counter])).digest()
        dbMask = dbMask + tmp_hash
        counter += 1
    
    #8.   Let DB = maskedDB \xor dbMask.

    DB = [a ^ b for (a, b) in zip(maskedDB, dbMask)]

    #print("DB", len(DB), type(DB))
    #for i in DB:
    #    print("{:4x}".format(i), end='')
    #print()

    #11.  Let salt be the last sLen octets of DB.

    salt = DB[-32:]
    #print("salt", len(salt), type(salt))

    #for i in salt:
    #    print("{:4x}".format(i), end='')
    #print()

    #12.  Let
    #
    #     M' = (0x)00 00 00 00 00 00 00 00 || mHash || salt ;
    #
    #  M' is an octet string of length 8 + hLen + sLen with eight
    #  initial zero octets.
    
    m_prime = b'\0\0\0\0\0\0\0\0' + hash_dtbs + bytes(salt)

    #13.  Let H' = Hash(M'), an octet string of length hLen.

    H_prime = hashlib.sha256(m_prime).digest()

    #print("H", len(H))
    #for x in H:
    #    print("{:2x}".format(x), end="")
    #print()

    #print("H_prime", len(H_prime))
    #for x in H_prime:
    #    print("{:2x}".format(x), end="")
    #print()

    # 14.  If H = H', output "consistent".  Otherwise, output
    #      "inconsistent".

    if H != H_prime:
        raise InvalidSignature

except InvalidSignature:
    sys.exit("FAIL: Signaturprüfung gibt ein FAIL zurück")
else:
    print("Signatur-von-Hand OK")

