#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, attr, typing, re, pyfiglet, shutil, secrets, requests, json
from base64 import b64encode, b64decode
from binascii import hexlify, unhexlify

import requests

url = 'http://127.0.0.1:9000/'

#params = dict(
#    origin='Chicago,IL',
#    destination='Los+Angeles,CA',
#    waypoints='Joplin,MO|Oklahoma+City,OK',
#    sensor='false'
#)
params = {
  "PublicKeyECIES" : "brainpoolP256r1 0x18dc1896a13cb25c2f9e8636d6cbfa45e0ff2d1e6f9c1ffd4d4267bd37d06619 0x57eec305a545892b6df120be69dc660724afb53cef68a637b127cb8f0287b919",
  "Signature" : "MEQCIH/YxPUCTZ55NH3aY8OlADp7UjYa9W+1481MtWglb2ClAiA8/9Z4+i9ngRzcqX5I+GLUDRELQif1eRh0ra+uM61Z0A==",
  "Certificate" : "MIICLTCCAdKgAwIBAgIUeN5zyf3J1eWE15vGYoG1QQUxpdEwCgYIKoZIzj0EAwIwazELMAkGA1UEBhMCREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGluMRAwDgYDVQQKDAdnZW1hdGlrMRAwDgYDVQQLDAdnZW1hdGlrMRYwFAYDVQQDDA1TR0QtSFNNIFNHRCAxMB4XDTE5MDUxOTA3MDE0NFoXDTIwMDUxODA3MDE0NFowazELMAkGA1UEBhMCREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGluMRAwDgYDVQQKDAdnZW1hdGlrMRAwDgYDVQQLDAdnZW1hdGlrMRYwFAYDVQQDDA1TR0QtSFNNIFNHRCAxMFowFAYHKoZIzj0CAQYJKyQDAwIIAQEHA0IABHSEFrqJjs1HrZ0Q1ldvj6nRCEaYzoc5F0KGo5cozqF5OTThAD2YQf9aVasR+5r+MTIAFnT6XyMLB2nAPZGsBiijUzBRMB0GA1UdDgQWBBSpscBUDiQmgBpEyfio0KrV1dNhQjAfBgNVHSMEGDAWgBSpscBUDiQmgBpEyfio0KrV1dNhQjAPBgNVHRMBAf8EBTADAQH/MAoGCCqGSM49BAMCA0kAMEYCIQCN7UIXzB623FvQe1dT+atF7OYpAHXdaTpO/OEzuotcLwIhAJiNjht3HOv43C+7hylkW4n0mcDYkv6ie0In5CEYUnCO"
 }

resp = requests.get(url=url, params=params)
data = resp.json() # Check the JSON Response Content documentation below

print(type(data))
print(data)

print("Response:", resp.text)
print("Headers:" , resp.headers)
print("HTTP-Status:", resp.status_code)

