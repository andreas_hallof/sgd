#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import zmq, os, sys, signal, json, re, secrets
from base64 import b64decode, b64encode
from binascii import hexlify, unhexlify
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from collections import defaultdict
#sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ecies') )
sys.path.append( os.getcwd() )
from ecies import ecies
from ableitungsregeln import ableitungsregeln


def CheckConfig(cfg):
    """
    Sanity-Check der wichtigsten Konfigurationsdaten

    Zukünftig sollte man hier eine json-Schemaprüfung der übergebenen
    Konfigurationsdaten machen.
    """
    assert (cfg['TCPPort']>1024 and cfg['TCPPort']<60000)

    for i in [ 'CertificateFile', 'PrivateKeyFile' ]: 
        if not os.path.exists(cfg[i]):
            sys.exit('certfile "{}" nicht gefunden.'.format(i))

    return True


CONFIG_FILE="config-hsm-worker.json"
if not os.path.exists(CONFIG_FILE):
    sys.exit('Konfigurationsdatei "{}" nicht gefunden.'.format(i))
with open(CONFIG_FILE, "rt") as f:
    config=json.load(f)
CheckConfig(config)

# So jetzt erzeuge und signiere ich mir meinen signierten SGD-HSM-ECIES-Schlüssel
with open(config['CertificateFile'], "rb") as f:
    cert=f.read()
    B_cert=b64encode(cert).decode()

context = zmq.Context()

socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:{}".format(config['TCPPort']))

Req=json.dumps(
         {'Command'     : 'GetPublicKey',
          'Certificate' : B_cert,
          'OCSPResponse': ""
         }
).encode()

for i in range(10000):
    print(i)
    socket.send(Req)
    message = socket.recv()
    print("Res", i, message.decode())
