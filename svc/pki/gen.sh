#! /bin/bash

if [ ! -z "$1" ]; then
    MAX=$1
else
    MAX=1000
fi

SECONDS=0

for i in $(seq 1 $MAX); do

    echo -n "$i "
    openssl ecparam -name brainpoolP256r1 -genkey -out key_$i.pem
    openssl req -x509 -key key_$i.pem -outform der \
        -out cert_$i.der -days 365 \
        -subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik-test/CN=sgd user $i"

done

echo "" 

SECONDS_ELAPSED=$SECONDS

echo benötigte Zeit um $MAX Schlüssel + Zertifikat zu erzeugen: $SECONDS_ELAPSED Sekunden

