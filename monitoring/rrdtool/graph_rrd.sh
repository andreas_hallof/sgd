#! /bin/bash


rrdtool graph ibm-sgd1.png --start -24h --title "IBM-SGD1" \
	--vertical-label "msec" DEF:tta=ibm-sgd1.rrd:tta:AVERAGE LINE1:tta#0000ff:"Time-to-Answer" 

rrdtool graph sgd2-via-ibm.png --start -24h --title "SGD2 via IBM-Gateway" \
	--vertical-label "msec" DEF:tta=sgd2-via-ibm.rrd:tta:AVERAGE LINE1:tta#0000ff:"Time-to-Answer" 
