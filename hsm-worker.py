#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import zmq, os, sys, signal, json, re, secrets
from base64 import b64decode, b64encode
from binascii import hexlify, unhexlify
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from collections import defaultdict
#sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ecies') )
sys.path.append( os.getcwd() )
from ecies import ecies
from ableitungsregeln import ableitungsregeln


def CheckConfig(cfg):
    """
    Sanity-Check der wichtigsten Konfigurationsdaten

    Zukünftig sollte man hier eine json-Schemaprüfung der übergebenen
    Konfigurationsdaten machen.
    """
    assert (cfg['TCPPort']>1024 and cfg['TCPPort']<60000)

    for i in [ 'CertificateFile', 'PrivateKeyFile' ]: 
        if not os.path.exists(cfg[i]):
            sys.exit('certfile "{}" nicht gefunden.'.format(i))

    return True


CONFIG_FILE="config-hsm-worker.json"
if not os.path.exists(CONFIG_FILE):
    sys.exit('Konfigurationsdatei "{}" nicht gefunden.'.format(i))
with open(CONFIG_FILE, "rt") as f:
    config=json.load(f)
CheckConfig(config)

# https://stackoverflow.com/questions/1112343/how-do-i-capture-sigint-in-python
def signal_handler(sig, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# So jetzt erzeuge und signiere ich mir meinen signierten SGD-HSM-ECIES-Schlüssel
with open(config['CertificateFile'], "rb") as f:
    cert=f.read()
    B_cert=b64encode(cert)


## Nun zu den SGD-HSM-Schlüsseln

# Das hier ist der Schlüssel (S1) aus A_17910
with open(config['PrivateKeyFile'], "rb") as f:
    S1_serialized_private_sig_key=f.read()
S1_private_sig_key = serialization.load_pem_private_key(
    S1_serialized_private_sig_key,
    password=None,
    backend=default_backend()
)
# Ableitungsschlüssel (S3) (also ein -- der erste -- Masterkey) 
S3_Ableitungsschlüssel={ "Wert" : b'12345678'*64, "Bezeichner" : "Ableitungsschluessel 1" }

# Das hier ist ein SGD-HSM-ECIES-Schlüssel (S4) aus A_17910
private_key      = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
PublicKeyECIES   = private_key.public_key()
pn               = PublicKeyECIES.public_numbers()
S_PublicKeyECIES = "{} {} {}".format(pn.curve.name, hex(pn.x), hex(pn.y))
# Das hier ist ein zum (S4)-Schlüssel zugehöriger (S5)-Schlüssel (A_17910)
AT_secret_key=secrets.token_hex(32).encode()
signature = S1_private_sig_key.sign(S_PublicKeyECIES.encode(), ec.ECDSA(hashes.SHA256()) )
B_signature=b64encode(signature)

S_PublicKeyECIES_A_17895 = '{ ' + '''
  "PublicKeyECIES" : "{}",
  "Signature" : "{}",
  "Certificate" : "{}"
'''.format(S_PublicKeyECIES, B_signature.decode(), B_cert.decode()) + ' } '

print("SGD-HSM ECIES-Schlüssel ist:\n", S_PublicKeyECIES_A_17895)

context = zmq.Context()
socket = context.socket(zmq.REP)
# https://stackoverflow.com/questions/6024003/why-doesnt-zeromq-work-on-localhost
socket.bind("tcp://127.0.0.1:{}".format(config['TCPPort']))

# ich benutze absichtlich nicht defaultdict
RequestCounter=defaultdict(int)
print("Waiting for Requests from RVE ...")
while True:
    #  Wait for next request from client
    raw_message = socket.recv()
    print("Received request:", raw_message)
    RequestCounter['Requests']+=1
    message=raw_message.decode()
    try:
        cmd=json.loads(message)
    except:
        socket.send(b'{ "Status" : "invalid internal request" }')
        continue
        
    if not 'Command' in cmd:
        socket.send(b'{ "Status" : "invalid internal request" }')
        continue

    if cmd['Command'] == 'GetPublicKey':
        RequestCounter['GetPublicKey']+=1
        socket.send(S_PublicKeyECIES_A_17895.encode())

    elif cmd['Command'] == 'GetAuthenticationToken':
        RequestCounter['GetAuthenticationToken']+=1

        if not 'PublicKeyECIES' in cmd:
            socket.send(b'{ "Status" : "invalid internal request" , "Info" : "no PublicKeyECIES" }')
            continue
        CPK=cmd['PublicKeyECIES']
        # xxx check PublicKey
        # xxx check dass der Hash zu einem meiner S4-Schlüssel passt
        if not 'Certificate' in cmd:
            socket.send(b'{ "Status" : "invalid internal request" , "Info" : "no Certificate" }')
        CC =b64decode(cmd['Certificate'])
        # xxx check signature

        if not 'EncryptedMessage' in cmd:
            socket.send(b'{ "Status" : "invalid internal request" , "Info" : "no EncryptedMessage" }')
            continue

        try:
            Plaintext=ecies.DecryptECIES(private_key, cmd['EncryptedMessage'])
        except Exception as e:
            print("ECIES: Decrypt: Exception:", str(e))
            socket.send(b'{ "Status" : "decryption FAIL" }')
            continue
        if len(Plaintext)!=len('Challenge') + 1 + 64 \
            or not Plaintext.startswith('Challenge '):
            socket.send(b'{ "Status" : "request not valid" }')
            continue
        A=CPK.encode()+CC
        AT_raw = HKDF(
            algorithm=hashes.SHA256(),
            length=32,
            salt=None,
            info=A,
            backend=default_backend()
        ).derive(AT_secret_key)
        H_raw=hashes.Hash(hashes.SHA256(), backend=default_backend())
        H_raw.update(A)
        H=hexlify(H_raw.finalize())

        AT='AT' + hexlify(AT_raw).decode()
        Response='Response '+Plaintext[10:] + ' '+ H.decode() + ' '+ AT
        print("Ich habe folgenden Klartext erzeugt:", Response)
        res=ecies.EncryptECIES(CPK, Response)
        print("Ich sende folgenden Ciphertext:", res)
        Antwort=json.dumps( { "Status" : "OK", "EncryptedMessage" : res } ).encode()

        socket.send(Antwort)
        RequestCounter['OK-GetAuthenticationToken']+=1

    elif cmd['Command'] == 'KeyDerivation':
        RequestCounter['KeyDerivation']+=1

        if not 'PublicKeyECIES' in cmd:
            socket.send(b'{ "Status" : "invalid internal request" , "Info" : "no PublicKeyECIES" }')
            continue
        CPK=cmd['PublicKeyECIES']
        if not 'Certificate' in cmd:
            socket.send(b'{ "Status" : "invalid internal request" , "Info" : "no Certificate" }')
        CC =b64decode(cmd['Certificate'])

        if not 'EncryptedMessage' in cmd:
            socket.send(b'{ "Status" : "invalid internal request" , "Info" : "no EncryptedMessage" }')
            continue
        
        try:
            Plaintext=ecies.DecryptECIES(private_key, cmd['EncryptedMessage'])
        except Exception as e:
            print("ECIES: Decrypt: Exception:" + str(e))
            socket.send(b'{ "Status" : "decryption FAIL" }')
            continue

        so=re.search('^(AT[A-Fa-f0-9]{64}) ([A-Fa-f0-9]{64}) KeyDerivation (r\d:.+)', Plaintext)
        if not so:
            socket.send(b'{ "Status" : "request not valid", "Info" : "(Regex-Auswertung)" }')
            continue

        AT=so.group(1)

        A=CPK.encode()+CC
        My_AT_raw = HKDF( algorithm=hashes.SHA256(), length=32, salt=None, info=A, 
                          backend=default_backend()).derive(AT_secret_key)
        My_AT='AT' + hexlify(My_AT_raw).decode()

        if AT!=My_AT:
            socket.send(b'{ "Status" : "request not valid", "Info" : "Authentisierungstoken falsch" }')
            continue

        Request_ID=so.group(2)
        Ableitungsregel=so.group(3)
        try: 
            X=ableitungsregeln.KeyDerivation(
                    Ableitungsregel,
                    cmd['Certificate'], 
                    S3_Ableitungsschlüssel
            )

        except Exception as e:
            print("KeyDerivation: Exception:" + str(e))
            socket.send(b'{ "Status" : "request not valid", "Info" : "1" }')
            continue
            
        Response=AT + " " + Request_ID + " OK-KeyDerivation " + X
        print("Ich habe folgenden Klartext erzeugt:", Response)
        res=ecies.EncryptECIES(CPK, Response)
        print("Ich sende folgenden Ciphertext:", res)
        Antwort=json.dumps( { "Status" : "OK", "EncryptedMessage" : res } ).encode()
        socket.send(Antwort)
        RequestCounter['OK-KeyDerivation']+=1

    elif cmd['Command'] == 'GetStatus':
        res='{ "Status" : "OK"'
        for x,y in RequestCounter.items():
            res+=', "{}" : "{}" '
        res+="}"
        socket.send(res.encode())

    else:
        socket.send(b'{ "Status" : "Ups" }')

