# Schlüsselgenerierungsdienst (SGD)

Diese Implementierung des SGD (vgl. gemSpec\_SGD bei [Spezifikationen](https://fachportal.gematik.de/spezifikationen/online-produktivbetrieb/))
dient der qualitivativen Überprüfung der Spezifikation (Vollständigkeit,
Widerspruchsfreiheit, Eindeutigkeit etc.).

Die Implementierung im AKTOR (ePA Aktensimulator) entstand unabhängig von
dieser Implementierung rein auf Grundlage der Spezifikation. Bis auf ein
kleines Kodierungsproblem (vgl. neu A\_18249 und A\_18250 in gemSpec\_SGD
Version 1.0.1) bestand auf Anhieb eine Kreuz-Interoperabilität zwischen beiden
SGD-Implementierungen:

1. der SGD-Client dieser Implementierung gegen den SGD-Server von AKTOR, und
2. der SGD-Client von AKTOR gegen den SGD-Server dieser Implememtierung.

## AKTOR-SGD

Zum Testen und für Entwicklungsarbeiten lieber den AKTOR-SGD verwenden, da
diese Implemtierung im Funktionsumfang vollständiger ist und besser gewartet
wird. Die Implementierung hier dient nur der Spezifikationsunterstützung.

## Lizenz
Die Implementierung steht und wird unter der GPL v3 Lizenz veröffentlicht (vgl.
beiliegende Datei "LICENSE-GPL\_V3").


## Author
Andreas Hallof
