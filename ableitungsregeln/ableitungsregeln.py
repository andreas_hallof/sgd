#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import secrets, re, base64
from binascii import hexlify, unhexlify
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF

def KeyDerivation(s : str, cert : str, masterkeys : dict):

    # Das ist der Algorithmus von "Tab_Kommandoabarbeitung_im_SGD-HSM" 
    # nach A_17922

    # (1) -- Check "^KeyDerivation " hat schon mein Aufrufer getan
    # (2) s wird mir übergeben -> nichts zu tun für mich hier
    # (3)
    if not re.match("r[123]", s):
        raise ValueError("Falsche Ableitungsvektor, 1")
    # (4)
    try:
        cert_der=base64.b64decode(cert)
        cert_obj=x509.load_der_x509_certificate(cert_der, default_backend())
        print("Stelle 1")
        for x in [ y.value for y in cert_obj.subject ]:
            print("Stelle 1")
            print(x)
            so=re.search('([aA]\d{9})', x)
            if so:
                KVNR=so.group(0)

        print("Stelle 1")
        # jetzt ist noch todo für (5)
    except Exception as e:
        print("Haaa ups", e)

    # (6)
    BEZ=masterkeys['Bezeichner']

    # (7)
    a_s=re.split(r':',s)
    l_a_s=len(a_s)
    # (8)
    assert l_a_s>0

    # (9)
    if a_s[0]=='r1' and l_a_s==2:
        # (9.1)
        assert KVNR!=''
        # (9.2)
        assert a_s[1]==KVNR
        # (9.2)
        RND=secrets.token_hex(32)
        # (9.3)
        a="r1:{}:{}:{}".format(RND, KVNR, BEZ)
        # (9.4)



        
    return "xxx"


if __name__=='__main__':

    print("xxx")

