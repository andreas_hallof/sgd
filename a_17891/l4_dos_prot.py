#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, hashlib, time

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature


H_SGD1 = b"39ee5f1765915640809456c22ecfcb070e3469c7f79eb4341bf14b407243f994"
H_SGD2 = b"af191ec5cd8247bc08a1b9618b38117294f5d0fd96c437e1d7bc6fd7012463db"

MY_LIMIT = 2*10**6
svc_cache = { "SVC" : 1 }

def reset_svc():
    global svc_cache

    svc_cache = {H_SGD1 : set(), H_SGD1+b'_' : 0}

def is_in_svc(public_key_ecies: bytes, signature : bytes, cert: bytes)-> bool:
    cache_item = hashlib.sha256(public_key_ecies + signature + cert).digest()
    # ich nehme als Beisiel an, dass ich bin ein SGD1 bin
    hsm_key_hash = public_key_ecies.split()[3]

    # der Nutzer referenziert in dem Fall ein alter S4-Schlüsselpaar
    # -> Fehlermeldung und Abbruch
    assert hsm_key_hash in svc_cache

    return cache_item in svc_cache[hsm_key_hash]

def add_svc(public_key_ecies: bytes, signature: bytes, cert: bytes):
    cache_item = hashlib.sha256(public_key_ecies + signature + cert).digest()
    # ich nehme als Beisiel an, dass ich bin ein SGD1 bin
    hsm_key_hash = public_key_ecies.split()[3]

    # der nutzer referenziert in dem Fall ein alter S4-Schlüsselpaar
    # -> Fehlermeldung und Abbruch
    assert hsm_key_hash in svc_cache

    if svc_cache[hsm_key_hash + b'_'] > MY_LIMIT:
        print("zu viele Elemente im Cache für S4-Schlüssel", hsm_key_hash)
        return False

    svc_cache[hsm_key_hash].add(cache_item)
    svc_cache[hsm_key_hash + b'_'] += 1

    return True

cert_counter = { }

def reset_cert_counter():
    global cert_counter

    cert_counter = {H_SGD1 : {}, H_SGD1+b'_' : 0}



if __name__ == '__main__':

    reset_svc()
    reset_cert_counter()

