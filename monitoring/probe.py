#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

#import os, json, hashlib, requests, sys, time
from base64 import b64decode
import json, requests, time

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature

def check_certificate(my_dict):
    "Gibt True/False zurück, ob es ein gültiges SGD-Zertifikat ist."

    with open("sgd-zertifikate.txt", "rt") as sgd_zertifikats_datei:
        while cert := sgd_zertifikats_datei.readline().rstrip():
            if my_dict['Certificate'] == cert:
                return True
    return False

def check_signature(my_dict):
    "Gibt True/False zurück, ob es die Signatur ok ist."

    try:
        sgd_cert = b64decode(my_dict['Certificate'])
        sgd_cert = x509.load_der_x509_certificate(sgd_cert, default_backend())
        sgd_pub_key = sgd_cert.public_key()
        sgd_pub_key.verify(
            b64decode(my_dict["Signature"].encode()),
            my_dict["PublicKeyECIES"].encode(),
            ec.ECDSA(hashes.SHA256())
        )
    except InvalidSignature:
        print("Signaturprüfung gibt FAIL")
        return False
    except Exception as myexception:
        # hier kommt man bspw. bei ungültiger base64-Kodierung -> dekodieren
        # hin
        print("andersartige Exception bei der Signaturprüfung", str(myexception))
        return False

    return True

def check_public_key(my_dict):
    # liegt der öffentlichen Schlüssel (PublicKeyECIES) auf der richtigen
    # Kurve (True/False)

    pub_key = my_dict["PublicKeyECIES"].split()
    if len(pub_key) != 3:
        return False
    if pub_key[0] != "brainpoolP256r1":
        return False

    x = int(pub_key[1], 16)
    y = int(pub_key[2], 16)

    p = 0xA9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377
    A = 0x7D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9
    B = 0x26DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6


    left = pow(y, 2, p)
    right = (pow(x, 3, p) + A*x + B) % p

    if left == right:
        return True

    return False


HttpsSession = requests.Session()

urls_and_logfilenames = {
        "https://tk.ibmepa.de/SGD1" : "ibm-sgd1.log",
        "https://tk.ibmepa.de/SGD2" : "sgd2-via-ibmgw.log"
        }

for myurl in urls_and_logfilenames:
    time_start = time.time()
    req = HttpsSession.post(myurl,
            json={"Command" : "GetPublicKey",
                  "Certificate" : "SGFsbG8gVGVzdAo=",
                  "OCSPResponse" : ""},
            timeout=25)
    time_end = time.time()
    time_diff = time_end - time_start

    print(req.status_code)
    mydata = req.json()
    mydata["connection"] = "OK" if req.status_code == requests.codes.ok else "FAIL"
    print(mydata)

    result = {
        "time_probe" : int(time_start),
        "cert_check" : check_certificate(mydata),
        "sig_check" : check_signature(mydata),
        "pub_key_check" : check_public_key(mydata),
        "connection" : mydata['connection'],
        "pub_key_hash" : mydata["PublicKeyECIES"].split()[1][2:4],
        "pub_key" : " ".join(mydata["PublicKeyECIES"].split()[1:]),
        "time" : time_diff
    }

    result_s = json.dumps(result)
    print(result_s)

    with open(urls_and_logfilenames[myurl], "at") as mylogfile:
        mylogfile.write(result_s + "\n")

