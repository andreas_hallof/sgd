#! /usr/bin/env bash

openssl ecparam -name brainpoolP256r1 -genkey -out egk-1-aut-key.pem
openssl ecparam -name brainpoolP256r1 -genkey -out smc-b-1-aut-key.pem

openssl req -x509 -key egk-1-aut-key.pem \
-outform der \
-out egk-1-aut.der -days 365 \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=1234567890/OU=A112102647/CN=eGK-1-AUT"

openssl req -x509 -key smc-b-1-aut-key.pem \
-outform der \
-out smc-b-1-aut-key.der -days 365 \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/CN=SMC-B-1 Testpraxis 1/OU=TI12345678"

