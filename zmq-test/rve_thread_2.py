#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import zmq, time, sys, random

WORKER1_TCP_PORT=6123

context = zmq.Context()

Error=False

#  Socket to talk to server
print("Connecting to hello world server…")
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:{}".format(WORKER1_TCP_PORT))

for request in range(10000):
    req="Req 2 {}".format(request).encode()
    print("Sending request", req)
    socket.send(req)

    time.sleep(random.random())

    #  Get the reply.
    message = socket.recv()
    print("Received", message)
    if not message.startswith(b'Ret Req 2 '):
        Error=True

if Error:
    print("UUUUUUUUUUUU")
