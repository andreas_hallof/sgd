# PoC A\_17891-\* 

Bei A\_17891-\* geht es darum, die Anzahl der Requests pro Client
auf Anwendungsebene zu limitieren. Auf Netzwerkebene kann ein SGD
den Großteil seiner Clients nicht (oder nur schlecht) unterscheiden.
Deshalb gibt es auf Anwendungsebene einen ergänzenden Mechanismus.

Wichtige Fakten sind:

1. im PublicKeyECIES des Client ist der Hashwert des "Ziel-(S4)" enthalten
2. der PublicKeyECIES ist an diesen (S4)-Schlüssel gebunden
3. die Anzahl der Signaturen, die für die Erzeugung eines gültigen
   PublicKeyECIES erfordlich sind, ist durch die beschränkte Lebensdauer der
   (S4)-Schlüssel limitiert

