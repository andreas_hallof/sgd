#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, attr, typing, re, pyfiglet, shutil, secrets, requests, json
from base64 import b64encode, b64decode
from binascii import hexlify, unhexlify
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives import serialization
#from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat



def banner(s):
    if not s:
        return
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    ## https://www.programcreek.com/python/example/96392/pyfiglet.Figlet
    #print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))
    # slant, basic, gothic, small, cybermedium
    print(pyfiglet.Figlet(font='slant', width=int(x.columns)).renderText(s))

if __name__=='__main__':

    banner("Schritt 0")
    #r = requests.get('https://github.com/timeline.json')
    r = requests.get('https://jsonplaceholder.typicode.com/todos/1')
    print("Response:", r.text)
    print("Headers:" , r.headers)
    print("HTTP-Status:", r.status_code)
    assert r.status_code == requests.codes.ok
    # das müsste dann als 'try' gemacht werden falls der server doch kein
    # json zurückliefert
    res=r.json()
    print(res)
    print(type(res))

    sys.exit(0)

    if type(res)!="<class 'dict'>":
        res=json.loads(res)
        print("Warning: MIME-Type stimmt evtl. immernoch nicht im Server")

    print("|",type(res),"|", res)

