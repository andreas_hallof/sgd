# Austauschformat

Hinweis: das Modul attr was in austauschformat.py ist aus dem Packet
[attrs](https://pypi.org/project/attrs/) was meist per default Teil einer
Python-Installation ist und nicht das aus
[attr](https://pypi.org/project/attr/).

    $ ./austauschformat.py
                     __  
                     \ \ 
     _________________\ \
    /_____/_____/_____/ /
                     /_/ 
                         

    Im Client (bspw. ePA-FdV) wurden lokal zufällig Akten- und Kontext-Schlüssel erzeugt:

    <?xml version="1.0" encoding="UTF-8">
    <epa:PHRKey insurant="A123456789">

        <!-- Aktenschlüssel --> 
        <RecordKey algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
            Nj9OixvhO2JKjtYEbQe8oetiQaiennKFJmQEJXsQVQo=
        </RecordKey> 

        <!-- Kontextschlüssel -->
        <ContextKey algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
            qyVQMtj3MwXRt8NOuQrNj3g5IPl49Ieami/+QVLzTkc=
        </ContextKey>

    </epa:PHRKey>

               __  
               \ \ 
     ___________\ \
    /_____/_____/ /
               /_/ 
                   


    ... Annahme: über die zwei SGD habe ich zwei AES-Schlüssel erhalten
    (hier als verwendete Beispielwerte b'12345678901234567890123456789012' und 
     b'A2345678901234567890123456789012')
    Damit verschlüssele ich nun zwei Mal hintereinander die PHRKey-Datenstruktur
    und erhalte eine EncryptedKeyContainer-Datenstruktur. 


                    __  
      ______________\ \ 
     /____/____/____/\ \
    /____/____/____/ / /
                    /_/ 
                        

    Erste Verschlüsselungsschicht mittels des vom SGD1 erhaltenen AES-256 Bit Schlüssels:

    <?xml version="1.0" encoding="UTF-8">
    <epa:EnryptedKeyContainer algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
        <epa:Ciphertext>
            Pp92TQ32EDYS5piirgL8zfDFomCs3EUeJYGZsGN0Yh+lwIUBQWbZy6ycmI283TaqVp/xhISJhtiO2IWuxczVXjUXYxW8KKkkV0DXUjEzqDEk/asmUiSKs8xvE4zj7lUIx4lmPBinU7/CgSGDX3s5py2MKI3Sy9FFZLuCWS6BzRR0lO9t+1tNNdUmU818BCfRd/K0HJUuCCO0mLmkphLgk5tLem/lWW/ztoWZZdLYEZ5UMNTCrGZ+ehuOGtzY7e/Nd3p/jEJi9PgEgQJLbKZ9Unn1dmkRB5BT228bQcX5n5fO50FgFjC3nG0cKPYVGEAR6ebObZckGhoa/cqlquJRmRuGuPPYjPXk1jC6l+6NrIK+8kwsBDPmgRk8fD6P0+0iChN2JeKfBTJ/6X1lrESK0ZQfxfahuHPxMFqbkotJbIbdlHNGx+9VjtS5TGRs9Qs/c4uxhCtkonuAipnzF3lFIoixtUPBYNrUGxQe2MafsFU2M+cCOjneQHIxMQZcOMzYJDdaaqub8yXHasKMTd4VO7Pu49mOeHj2jD0qd/vgRpDSzQaJD3mxVeXTHvsJv4jjzL+IPlJkAsU0y94GPOgVoHGJImtYBbq5PTxS8NqY7Z8=
        </epa:Ciphertext>
        <epa:AssociatedData>
            cjI6N2Y4Zjc3MDAzZGJhYjQ5YzNhNGUzMmY0NDcyNmY5MjMyNGQyOTJmYTY2OGZkZTVlYmMzNDI0Mzk3OTg2YmU5OTpBMTIzNDU2Nzg5OjItMjBhMTIwMS0wMDE6QWt0ZW5zeXN0ZW0gYSwgU0dEMSwgQmV6ZWljaG5lciAyMDIwLTE=
        </epa:AssociatedData>
    </epa:EncryptedKeyContainer>

                    __  
      ______________\ \ 
     /____/____/____/\ \
    /____/____/____/ / /
                    /_/ 
                        

    Zweite Verschlüsselungsschicht mittels des vom SGD2 erhaltenen AES-256 Bit Schlüssels:

    <?xml version="1.0" encoding="UTF-8">
    <epa:EnryptedKeyContainer algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
        <epa:Ciphertext>
            WCcrk47XqjqR2YYzwNUSqP8taNaxMGxVgm3RMIPmKPAyFGEfjG78fo/ajvYp5wkF6Crz599adr1rXJy9Zznt9YP6BTU34Oe1Rk6fQxYDdagWn8+TaS7qpgxq0jtgWPHXczeqkwiO1pldlV3IX7+7OxJnv6Dyl9AKRrm+akOp2k2vNDKneOVvC2+82Rj6aH3vfJx081uJMdWvQLTEScVKKk1/n7KX9GA3VQR1qOgBVaw6OYKDJNFg2qdk3qzXRGAnUUVJB3HIciygNm0fgGazMH1Xi7DqvUzhtdloPWvaXkVP0dgqQxW4+JO/U8XOMIlwFOgiSpf3DtNzSgovtFvXMcSGcUCNateikE0DD3UI06/UxIVCmaehZK+VHMdSxFv+TCDD5gDqe+KPJOia7BRLOFOieH4kYpqXKzmC+1ZxIfSLIh4d0ntQUElYYIOF+KFc6bx3bISyamGbStLD1AneAEjI0AVpifcp3D88x/xZ/aVvnALFU2eMcP412dw8F/ubvbqUPcCusL6dzn0wwubkAz/i7bfBfdoGfTan078U/WI1zmIoExIo4sJ+CPyt6uBfPndy4q+jApw2ppxb3XR8JUJWT6D0UANDmpQSLJA/ZAV7U6mJsVLdtKgWkhov3xQH7MFwC3Bamosg9sXSjzwxgnPDgIBmNvhrn22KZhD7x9L5r/A5ATDyDteL9lWDIH9ge2RzFVn/GAW/0QUeM2keoVfvq28afYZNyE79Al//X7Oi97MCkl1ZG9R+w42Ru01iLa4nQSQyZsyhlCZID6nOTdgviq2G8gwRcisJv06nP6eEA7JEDlCm1H5wG0aO7ArsMHzT3FXmYIAzuiCCdznU1yFF5TcpZdEF6mDx4d2opDRIXQfXCB9A6QZJjYP7yD6wkvXiuYnr97HPf1xwYpAhtfTsQ5SuTIGZEP0yDSkhzIEI7OB7SxVOJRkef6C+SsZmdmmNv2dvU2Iuhc3rkTr5BNMGyzcOXB34HwVJsQhZoa+W4WkyRJu1JX8RiPQgcagBEwwpLOV0A921V/TZmgd+gaOAF9I1ETu9mSkoWCVMSfMNrsHWyuWoYarb1gkY+M3l/MtDIYbWXv0uIZGhx694ynMyLGsJeu0ihFGSosGl69qhwWziMWo2Ym6uNX3Vlyg5dIJn4DgxmDQ4xnHU2zxo1zsbyATUbERsNG2K93peR3mkL4Mi/LFq3OlriCdMD+j8z4LHlCYC1uGRxI9DPC7Fip4nGSyMmCWb8d0qdnSqLcbCEqdYHGoHStPWYm9npWuoVc7VH7vmh/YOovjP4LO44PrQMJxU8dfh1HPsBLfNzHRhDibixCfVugILMi58wwRTIz95fOZxoobDX8NvsDt7khI0cE97NxFhiUJnj7WV+JPBXZTBcd9q0vWx4thx06AMSIdYx6vWLbPsvJeFpO4YJDWM7N2oDKuvamZh7ko=
        </epa:Ciphertext>
        <epa:AssociatedData>
            cjI6N2Y4Zjc3MDAzZGJhYjQ5YzNhNGUzMmY0NDcyNmY5MjMyNGQyOTJmYTY2OGZkZTVlYmMzNDI0Mzk3OTg2YmU5OTpBMTIzNDU2Nzg5OjItMjBhMTIwMS0wMDE6QWt0ZW5zeXN0ZW0gYSwgU0dEMSwgQmV6ZWljaG5lciAyMDIwLTE= cjI6NWQ2MWQyZTExNTJiNjcxMWJlOTg0OTZjZDZmMGM5YWJkZTRjYzNiMzIwYjRiYWYxMjc2ZTU1MmFhZGU4MDkxMzpBMTIzNDU2Nzg5OjItMjBhMTIwMS0wMDE6U0dEMiBNYXN0ZXJrZXkgMjAyMC0x
        </epa:AssociatedData>
    </epa:EncryptedKeyContainer>

               __  
               \ \ 
     ___________\ \
    /_____/_____/ /
               /_/ 
                   

    Ins Aktensystem würde ich jetzt folgende Datenstruktur einlagern:

    <?xml version="1.0" encoding="UTF-8">
    <epa:EnryptedKeyContainer algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
        <epa:Ciphertext>
            WCcrk47XqjqR2YYzwNUSqP8taNaxMGxVgm3RMIPmKPAyFGEfjG78fo/ajvYp5wkF6Crz599adr1rXJy9Zznt9YP6BTU34Oe1Rk6fQxYDdagWn8+TaS7qpgxq0jtgWPHXczeqkwiO1pldlV3IX7+7OxJnv6Dyl9AKRrm+akOp2k2vNDKneOVvC2+82Rj6aH3vfJx081uJMdWvQLTEScVKKk1/n7KX9GA3VQR1qOgBVaw6OYKDJNFg2qdk3qzXRGAnUUVJB3HIciygNm0fgGazMH1Xi7DqvUzhtdloPWvaXkVP0dgqQxW4+JO/U8XOMIlwFOgiSpf3DtNzSgovtFvXMcSGcUCNateikE0DD3UI06/UxIVCmaehZK+VHMdSxFv+TCDD5gDqe+KPJOia7BRLOFOieH4kYpqXKzmC+1ZxIfSLIh4d0ntQUElYYIOF+KFc6bx3bISyamGbStLD1AneAEjI0AVpifcp3D88x/xZ/aVvnALFU2eMcP412dw8F/ubvbqUPcCusL6dzn0wwubkAz/i7bfBfdoGfTan078U/WI1zmIoExIo4sJ+CPyt6uBfPndy4q+jApw2ppxb3XR8JUJWT6D0UANDmpQSLJA/ZAV7U6mJsVLdtKgWkhov3xQH7MFwC3Bamosg9sXSjzwxgnPDgIBmNvhrn22KZhD7x9L5r/A5ATDyDteL9lWDIH9ge2RzFVn/GAW/0QUeM2keoVfvq28afYZNyE79Al//X7Oi97MCkl1ZG9R+w42Ru01iLa4nQSQyZsyhlCZID6nOTdgviq2G8gwRcisJv06nP6eEA7JEDlCm1H5wG0aO7ArsMHzT3FXmYIAzuiCCdznU1yFF5TcpZdEF6mDx4d2opDRIXQfXCB9A6QZJjYP7yD6wkvXiuYnr97HPf1xwYpAhtfTsQ5SuTIGZEP0yDSkhzIEI7OB7SxVOJRkef6C+SsZmdmmNv2dvU2Iuhc3rkTr5BNMGyzcOXB34HwVJsQhZoa+W4WkyRJu1JX8RiPQgcagBEwwpLOV0A921V/TZmgd+gaOAF9I1ETu9mSkoWCVMSfMNrsHWyuWoYarb1gkY+M3l/MtDIYbWXv0uIZGhx694ynMyLGsJeu0ihFGSosGl69qhwWziMWo2Ym6uNX3Vlyg5dIJn4DgxmDQ4xnHU2zxo1zsbyATUbERsNG2K93peR3mkL4Mi/LFq3OlriCdMD+j8z4LHlCYC1uGRxI9DPC7Fip4nGSyMmCWb8d0qdnSqLcbCEqdYHGoHStPWYm9npWuoVc7VH7vmh/YOovjP4LO44PrQMJxU8dfh1HPsBLfNzHRhDibixCfVugILMi58wwRTIz95fOZxoobDX8NvsDt7khI0cE97NxFhiUJnj7WV+JPBXZTBcd9q0vWx4thx06AMSIdYx6vWLbPsvJeFpO4YJDWM7N2oDKuvamZh7ko=
        </epa:Ciphertext>
        <epa:AssociatedData>
            cjI6N2Y4Zjc3MDAzZGJhYjQ5YzNhNGUzMmY0NDcyNmY5MjMyNGQyOTJmYTY2OGZkZTVlYmMzNDI0Mzk3OTg2YmU5OTpBMTIzNDU2Nzg5OjItMjBhMTIwMS0wMDE6QWt0ZW5zeXN0ZW0gYSwgU0dEMSwgQmV6ZWljaG5lciAyMDIwLTE= cjI6NWQ2MWQyZTExNTJiNjcxMWJlOTg0OTZjZDZmMGM5YWJkZTRjYzNiMzIwYjRiYWYxMjc2ZTU1MmFhZGU4MDkxMzpBMTIzNDU2Nzg5OjItMjBhMTIwMS0wMDE6U0dEMiBNYXN0ZXJrZXkgMjAyMC0x
        </epa:AssociatedData>
    </epa:EncryptedKeyContainer>

                     __  
                     \ \ 
     _________________\ \
    /_____/_____/_____/ /
                     /_/ 
                         


    ... wenn man die ePA-FdV man nächsten Tag wieder nutzt, würde man sich über die
    beiden SGD die beiden SGD-Schlüssel berechnen lassen und die aus dem
    Aktensystem erhaltene EncryptedKeyContainer-Datenstruktur wieder entschlüsseln
    ...  

    <?xml version="1.0" encoding="UTF-8">
    <epa:PHRKey insurant="A123456789">

        <!-- Aktenschlüssel --> 
        <RecordKey algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
            Nj9OixvhO2JKjtYEbQe8oetiQaiennKFJmQEJXsQVQo=
        </RecordKey> 

        <!-- Kontextschlüssel -->
        <ContextKey algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
            qyVQMtj3MwXRt8NOuQrNj3g5IPl49Ieami/+QVLzTkc=
        </ContextKey>

    </epa:PHRKey>

