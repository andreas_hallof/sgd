#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

from binascii import hexlify
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF

def Ableitungsschlüsselprüfwert(Schlüssel : bytearray) -> str:
    """
    Berechnet, wie in A_20976 definiert, einen
    Ableitungsschlüsselprüfwert eines S3-Schlüssels
    """
    A = HKDF(algorithm=hashes.SHA256(), length=32, salt=None,
             info=b"Ableitungsschluesselpruefwert-Schluessel-S3",
             backend=default_backend()
             ).derive(Schlüssel)
    return hexlify(A).decode()

if __name__=='__main__':

    Schlüssel_S3 = (("S3-Schlüssel SGD xyz Q4 2020", b"\x00"*600),
                    ("S3-Schlüssel SGD xyz Q2 2021", b"\x00\x01"*300),
                    ("S3-Schlüssel SGD xyz Q4 2021", b"\x00\x01\x02\x04"*150),
                    ("S3-Schlüssel SGD xyz Q2 2022", b"\x01"*600))

    # nur aus Spass, BEGINN
    from math import ceil

    # Annahme: mein Hardware-Zufallsgenerator würde eine Ausgabeentropie
    # von 0.86 pro Bit besitzen.
    RND_SRC_EST_ENT = 0.86
    MIN_ENT = 512 # Mindestentropie nach A_17876
    MIN_LEN = ceil(MIN_ENT / RND_SRC_EST_ENT)
    for (S3_Name, Schlüsselwert) in Schlüssel_S3:
        assert len(Schlüsselwert) >= MIN_LEN
    # ENDE

    for (S3_Name, Schlüsselwert) in Schlüssel_S3:
        print(f"{S3_Name}:", Ableitungsschlüsselprüfwert(Schlüsselwert))

