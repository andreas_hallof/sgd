#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, json, sys, time
import matplotlib.pyplot as plt

logfile_name = "ibm-sgd1.log" if len(sys.argv) == 1 else sys.argv[1]

data = []
p = 0xA9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377
p1= 0xa9

with open(logfile_name, "rt") as mylogfile:
    while line := mylogfile.readline():
        entry = json.loads(line)
        data.append(int(entry["pub_key_hash"], 16))

n, bins, patches = plt.hist(data, 20, facecolor='blue', alpha=0.75, rwidth=0.5)

plt.savefig(os.path.basename(logfile_name)+"-histogramm.png")

