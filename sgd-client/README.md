# mit locking

mit locking und den selben zmq-context im Server (RVE):

(zeiten für 10.000 requests, d. h. fdv macht 10^5 GetPublicKey-Requests)

    [a@h sgd-client]$ time ./fdv.py >/dev/null

    real    0m40,706s
    user    0m25,362s
    sys     0m0,986s

    [a@h sgd-client]$ time ./fdv.py >/dev/null

    real    0m40,162s
    user    0m25,026s
    sys     0m1,020s
    [a@h sgd-client]$ time ./fdv.py >/dev/null

    real    0m39,580s
    user    0m24,785s
    sys     0m0,948s
    [a@h sgd-client]$ time ./fdv.py >/dev/null

    real    0m40,161s
    user    0m25,378s
    sys     0m0,902s


# ohne locking

ohne locking (immer ein neuer zmq-context pro request in der RVE):

    real    0m53,140s
    user    0m27,123s
    sys     0m1,342s

# ohne hsm-worker-kommunikation

ich kommentiere die kommunikation mit dem hsm-worker in der rve raus.
D. h., Zeiten ohne den IPC-ZMQ-Overhead. Der Unterschied zwischen 40s (s. o.)
und 37 ist nicht sonderlich groß.

    [a@h sgd-client]$ time ./fdv.py >/dev/null

    real    0m37,047s
    user    0m24,650s
    sys     0m0,873s
    [a@h sgd-client]$ time ./fdv.py >/dev/null

    real    0m36,963s
    user    0m24,475s
    sys     0m0,886s

