# Testvektoren für A\_20976

Nach A\_20976 muss ein SGD-HSM für die Ableitungsschlüssel S3
"Ableitungsschlüsselprüfwerte" berechnen und "ausgeben" können (also über einen
API-Call abrufbar machen). Für A\_20976 gibt es hier 4 Testvektoren (in
test\_vektoren.py sieht man die Beispielschlüsselwerte für die S3 Schlüssel).


    [a@h ableitungsschluesselpruefwert]$ ./test_vektoren.py
    S3-Schlüssel SGD xyz Q4 2020: 18c6f1d8cf7687e12452eb9a25aaa1b3490b667f41311de695f9f801728bf39e
    S3-Schlüssel SGD xyz Q2 2021: a39d2ece4eecf0be50cc2997495c928bfe0a2f27a4e3f09684c20bc01f898d09
    S3-Schlüssel SGD xyz Q4 2021: 8cb54c9ef8ea648a2a50e3bba69dcf0db3674021bae57b2e1ecba66b638d8b7e
    S3-Schlüssel SGD xyz Q2 2022: 9f9aa4c4810adfbd3843ab2a99c6d6d21175c9e397f3a2ae5657005a4ce0afad

