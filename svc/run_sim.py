#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import base64, cbor, os, sys, hashlib, time

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature


H_SGD1 = b"39ee5f1765915640809456c22ecfcb070e3469c7f79eb4341bf14b407243f994"
H_SGD2 = b"af191ec5cd8247bc08a1b9618b38117294f5d0fd96c437e1d7bc6fd7012463db"

MY_LIMIT = 2*10**6
svc_cache = { "SVG" : 1 }

def reset_svc():
    global svc_cache

    svc_cache = {H_SGD1 : set(), H_SGD1+b'_' : 0}

def is_in_svc(public_key_ecies: bytes, signature : bytes, cert: bytes)-> bool:
    cache_item = hashlib.sha256(public_key_ecies + signature + cert).digest()
    # ich nehme als Beisiel an, dass ich bin ein SGD1 bin
    hsm_key_hash = public_key_ecies.split()[3]

    # der Nutzer referenziert in dem Fall ein alter S4-Schlüsselpaar
    # -> Fehlermeldung und Abbruch
    assert hsm_key_hash in svc_cache

    return cache_item in svc_cache[hsm_key_hash]

def add_svc(public_key_ecies: bytes, signature: bytes, cert: bytes):
    cache_item = hashlib.sha256(public_key_ecies + signature + cert).digest()
    # ich nehme als Beisiel an, dass ich bin ein SGD1 bin
    hsm_key_hash = public_key_ecies.split()[3]

    # der nutzer referenziert in dem Fall ein alter S4-Schlüsselpaar
    # -> Fehlermeldung und Abbruch
    assert hsm_key_hash in svc_cache

    if svc_cache[hsm_key_hash + b'_'] > MY_LIMIT:
        print("zu viele Elemente im Cache für S4-Schlüssel", hsm_key_hash)
        return False

    svc_cache[hsm_key_hash].add(cache_item)
    svc_cache[hsm_key_hash + b'_'] += 1

    return True

def run_simulation(filename: str):

    if not os.path.exists(filename):
        sys.exit("File " + filename + " not found")

    my_requests = cbor.loads(open(filename, "rb").read())

    start = time.time()
    for req in my_requests:
        cert = x509.load_der_x509_certificate(base64.b64decode(req[2]), default_backend())
        public_key = cert.public_key()
        try:
            public_key.verify(req[1], req[0], ec.ECDSA(hashes.SHA256()))
        except InvalidSignature:
            sys.exit("Signatur ungültig!")

    time_without_svc = time.time()-start
    print("Simulation", filename, "ohne SVC", time_without_svc)

    reset_svc()

    start = time.time()
    for req in my_requests:
        if not is_in_svc(req[0], req[1], req[2]):
            cert = x509.load_der_x509_certificate(base64.b64decode(req[2]), default_backend())
            public_key = cert.public_key()
            try:
                public_key.verify(req[1], req[0], ec.ECDSA(hashes.SHA256()))
            except InvalidSignature:
                sys.exit("Signatur ungültig!")
            add_svc(req[0], req[1], req[2])
        else:
            # ok cache-hit
            pass

    time_with_svc = time.time()-start
    print("Simulation", filename, "mit SVC", time_with_svc)
    print("Ratio", time_without_svc / time_with_svc)

    print("aktueller Speicherverbrauch Cache", sys.getsizeof(svc_cache[H_SGD1]), "Bytes")

    return True



if __name__ == '__main__':

    reset_svc()

    run_simulation("sim-0-p.cbor")
    run_simulation("sim-10-p.cbor")
    run_simulation("sim-90-p.cbor")

