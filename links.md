# merker für links

## zmq
http://zguide.zeromq.org/page:all
https://github.com/jverhoeven/pyzmqrpc
http://zguide.zeromq.org/py:hwclient
http://zguide.zeromq.org/py:hwserver
https://pyzmq.readthedocs.io/en/latest/index.html
http://lucumr.pocoo.org/2012/6/26/disconnects-are-good-for-you/


## "flask"-nahe task-scheduler
https://github.com/smitchell556/cuttlepool#faq
https://github.com/smitchell556/cuttlepool#how-to-guide
https://cuttlepool.readthedocs.io/en/latest/api.html
http://flask.pocoo.org/
https://pypi.org/project/Flask-ZMQ/#files

## json-processing
https://www.programcreek.com/python/example/64945/flask.request.get_json

## requests.Session()
https://blog.insightdatascience.com/learning-about-the-http-connection-keep-alive-header-7ebe0efa209d

