#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import attr, os, pyfiglet, re, shutil
from base64 import b64encode, b64decode
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

PHRKeyFormat = '''\
<?xml version="1.0" encoding="UTF-8">
<epa:PHRKey insurant="{}">

    <!-- Aktenschlüssel --> 
    <RecordKey algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
        {}
    </RecordKey> 

    <!-- Kontextschlüssel -->
    <ContextKey algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
        {}
    </ContextKey>

</epa:PHRKey>
'''

EncryptedKeyContainerFormat = '''\
<?xml version="1.0" encoding="UTF-8">
<epa:EnryptedKeyContainer algorithm="http://www.w3.org/2009/xmlenc11#aes256-gcm">
    <epa:Ciphertext>
        {}
    </epa:Ciphertext>
    <epa:AssociatedData>
        {}
    </epa:AssociatedData>
</epa:EncryptedKeyContainer>
'''

# https://www.attrs.org/en/latest/types.html
@attr.s(auto_attribs=True)
class PHRKey():

    insurant: str = None
    RecordKey: bytes = None
    ContextKey: bytes = None

    def __str__(self):

        xstr = lambda s: '' if s is None else str(s)

        return PHRKeyFormat.format(
            xstr(self.insurant),
            xstr(b64encode(self.RecordKey).decode()),
            xstr(b64encode(self.ContextKey).decode())
         )

    def ExportEncryptedKeyContainer(self, key1: bytes, key2: bytes, ad1: bytes, ad2: bytes, DEBUG=False):

        if not key1 or len(key1) != 32: raise ValueError("Error with key1")
        if not key2 or len(key2) != 32: raise ValueError("Error with key2")
        if not ad1: raise ValueError("Error with ad1")
        if not ad2: raise ValueError("Error with ad2")

        iv = os.urandom(12)
        iv_plus_ct = iv + AESGCM(key1).encrypt(iv, self.__str__().encode(), ad1)
        pt = EncryptedKeyContainerFormat.format(
            b64encode(iv_plus_ct).decode(),
            b64encode(ad1).decode()
        )
        if DEBUG:
            banner("===>")
            print("Erste Verschlüsselungsschicht mittels des vom SGD1 erhaltenen AES-256 Bit Schlüssels:\n")
            print(pt)

        iv = os.urandom(12)
        iv_plus_ct = iv + AESGCM(key2).encrypt(iv, pt.encode(), ad1 + ad2)

        pt = EncryptedKeyContainerFormat.format(
            b64encode(iv_plus_ct).decode(),
            b64encode(ad1).decode() + " " + b64encode(ad2).decode()
        )
        if DEBUG:
            banner("===>")
            print("Zweite Verschlüsselungsschicht mittels des vom SGD2 erhaltenen AES-256 Bit Schlüssels:\n")
            print(pt)

        return pt

    def ImportEncryptedKeyContainer(self, key1: bytes, key2: bytes, EKC: str):

        if not key1 or len(key1) != 32: raise ValueError("Error with key1")
        if not key2 or len(key2) != 32: raise ValueError("Error with key2")
        if not EKC: raise ValueError("Error with EKC")

        find_result = re.findall(
            r'<epa:EnryptedKeyContainer\s+[Aa]lgorithm="([^"]+)".+' +
            r'<epa:Ciphertext>\s*([^>]+)</epa:Ciphertext>\s*<epa:AssociatedData>\s*([^ ]+)\s+([^< ]+)',
            EKC,
            re.M + re.S
        )[0]
        if not find_result:
            raise ValueError("EKC has wrong format (1/2).")

        (AlgoID, B_iv_plus_ct, B_ad1, B_ad2) = find_result
        assert AlgoID == 'http://www.w3.org/2009/xmlenc11#aes256-gcm'
        iv_plus_ct = b64decode(B_iv_plus_ct)
        assert len(iv_plus_ct) > (12+16)
        (iv, ct) = (iv_plus_ct[:12], iv_plus_ct[12:])
        (ad1, ad2) = [b64decode(i) for i in [B_ad1, B_ad2]]
        pt = AESGCM(key2).decrypt(iv, ct, ad1 + ad2).decode()

        so = re.search(
            r'<epa:EnryptedKeyContainer\s+[Aa]lgorithm="([^"]+)".+' +
            r'<epa:Ciphertext>\s*([^>]+)</epa:Ciphertext>\s*<epa:AssociatedData>\s*([^ <]+)',
            pt,
            re.M + re.S
        )
        if not so:
            raise ValueError("EKC has wrong format (2/2).")

        AlgoID = so.group(1)
        B_iv_plus_ct = so.group(2)
        B_ad1 = so.group(3)
        assert AlgoID == 'http://www.w3.org/2009/xmlenc11#aes256-gcm'
        iv_plus_ct = b64decode(B_iv_plus_ct)
        assert len(iv_plus_ct) > (12+16)
        (iv, ct) = (iv_plus_ct[:12], iv_plus_ct[12:])
        inner_ad1 = b64decode(B_ad1)
        assert inner_ad1 == ad1
        pt = AESGCM(key1).decrypt(iv, ct, inner_ad1).decode()

        so = re.search(r'<(?:[^:]+:)PHRKey\s+insurant="([^"]+)"', pt, re.M + re.S)
        if not so:
            raise ValueError("PHRKey has wrong format (1/3).")
        self.insurant = so.group(1)

        so = re.search(
            r'<RecordKey\s+[Aa]lgorithm="([^"]+)"\s*>\s*([^>]+)\s*',
            pt, 
            re.M + re.S
        )
        if not so:
            raise ValueError("PHRKey has wrong format (2/3).")
        AlgoID = so.group(1)
        B_RecordKey = so.group(2)
        assert AlgoID == 'http://www.w3.org/2009/xmlenc11#aes256-gcm'
        self.RecordKey = b64decode(B_RecordKey)

        so = re.search(
            r'<ContextKey\s+[Aa]lgorithm="([^"]+)"\s*>\s*([^>]+)\s*',
            pt,
            re.M + re.S
        )
        if not so:
            raise ValueError("PHRKey has wrong format (3/3).")

        AlgoID = so.group(1)
        B_ContextKey = so.group(2)
        assert AlgoID == 'http://www.w3.org/2009/xmlenc11#aes256-gcm'
        self.ContextKey = b64decode(B_ContextKey)

        return True

def banner(s):
    if not s:
        return
    #rows, columns = os.popen('stty size', 'r').read().split()
    x = shutil.get_terminal_size((80, 24))

    ## https://www.programcreek.com/python/example/96392/pyfiglet.Figlet
    #print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))
    # slant, basic, gothic, small, cybermedium

    print(pyfiglet.Figlet(font='slant', width=int(x.columns)).renderText(s))


############ MAIN ##################

if __name__ == '__main__':

    banner("--->")
    print("Im Client (bspw. ePA-FdV) wurden lokal zufällig Akten- und Kontext-Schlüssel erzeugt:\n")
    x = PHRKey(
        insurant="A123456789",
        RecordKey=b64decode("Nj9OixvhO2JKjtYEbQe8oetiQaiennKFJmQEJXsQVQo="),
        ContextKey=b64decode("qyVQMtj3MwXRt8NOuQrNj3g5IPl49Ieami/+QVLzTkc=")
    )
    print(x)

    banner("-->")
    print("""
... Annahme: über die zwei SGD habe ich zwei AES-Schlüssel erhalten
(hier als verwendete Beispielwerte b'12345678901234567890123456789012' und 
 b'A2345678901234567890123456789012')
Damit verschlüssele ich nun zwei Mal hintereinander die PHRKey-Datenstruktur
und erhalte eine EncryptedKeyContainer-Datenstruktur. 

""")

    EncryptedKeyContainer = x.ExportEncryptedKeyContainer(
        key1=b'12345678901234567890123456789012',
        key2=b'A2345678901234567890123456789012',
        ad1=b"r2:7f8f77003dbab49c3a4e32f44726f92324d292fa668fde5ebc3424397986be99:A123456789:2-20a1201-001:Aktensystem a, SGD1, Bezeichner 2020-1",
        ad2=b"r2:5d61d2e1152b6711be98496cd6f0c9abde4cc3b320b4baf1276e552aade80913:A123456789:2-20a1201-001:SGD2 Masterkey 2020-1",
        DEBUG=True
    )

    banner("-->")
    print("Ins Aktensystem würde ich jetzt folgende Datenstruktur einlagern:\n")
    print(EncryptedKeyContainer)

    banner("--->")
    print("""
... wenn man die ePA-FdV man nächsten Tag wieder nutzt, würde man sich über die
beiden SGD die beiden SGD-Schlüssel berechnen lassen und die aus dem
Aktensystem erhaltene EncryptedKeyContainer-Datenstruktur wieder entschlüsseln
...  
""")

    x.ImportEncryptedKeyContainer(
        key1=b'12345678901234567890123456789012',
        key2=b'A2345678901234567890123456789012',
        EKC=EncryptedKeyContainer
    )
    print(x)

