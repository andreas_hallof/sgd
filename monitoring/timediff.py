#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, json, sys, time

logfile_name = "ibm-sgd1.log" if len(sys.argv) == 1 else sys.argv[1]

with open(logfile_name, "rt") as mylogfile:
    last_time = None
    while line := mylogfile.readline():
        mydata = json.loads(line)
        if not last_time:
            last_time = mydata["time_probe"]
        else:
            mydiff = mydata["time_probe"] - last_time
            print(mydiff, mydiff//60)
            last_time = mydata["time_probe"]

