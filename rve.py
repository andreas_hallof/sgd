#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, binascii, hashlib, io, re, json, zmq, threading
from flask import Flask, request, Response, jsonify
from base64 import b64decode, b64encode

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature, InvalidTag

app = Flask(__name__)
config = {}


@app.route('/', methods=['GET', 'POST'])
def Connection_Handler_Root():
    return jsonify({ "Status" : "invalid Request", "Comment" : "Use URL /SDG1 or /SGD2" })


@app.route('/SGD<int:sgd_type>', methods=['GET', 'POST'], defaults={'sgd_type' : 1})
def Connection_Handler(sgd_type):

    if request.method == 'GET':
        return jsonify({ "Status" : "invalid Request" });
    elif request.method == 'POST':
        if request.content_type.startswith('application/json'):
            data=request.get_json(silent=True)
            if data is None:
                return jsonify({ "Status" : "invalid Request" });
                
            print(data)
            # todo: request check-en
            with HSM_1_lock:
                HSM_1_socket.send(json.dumps(data).encode())
                res=HSM_1_socket.recv()
            data=res.decode()

            return jsonify(data)

        return jsonify({ "Status" : "invalid Request" });
    else:
        print(request.content_type);
        return jsonify({ "Status" : "invalid Request" });


    # End of ConnectionHandler()

def CheckConfig(cfg):
    """ 
    Sanity-Check der wichtigsten Konfigurationsdaten

    Zukünftig sollte man hier eine json-Schemaprüfung der übergebenen
    Konfigurationsdaten machen.
    """
    assert (cfg['HSM1TCPPort']>1024 and cfg['HSM1TCPPort']<60000)

    return True

if __name__ == '__main__':

    CONFIG_FILE="config-rve.json"
    if not os.path.exists(CONFIG_FILE):
        sys.exit("Datei {} nicht gefunden.".format(CONFIG_FILE))
    with open(CONFIG_FILE, "rt") as f:
        config=json.load(f)
    CheckConfig(config)

    HSM_1_context = zmq.Context()
    HSM_1_socket = HSM_1_context.socket(zmq.REQ)
    HSM_1_socket.connect("tcp://localhost:{}".format(config['HSM1TCPPort']))
    HSM_1_lock=threading.Lock()

 
    MY_PORT=9000
    if not('HOSTNAME' in os.environ) or os.getenv('HOSTNAME')=='t':
        print("Produktiv-Server!")
        MY_DEBUG=False
    else:
        print("Nicht Produktiv-Server => Debug on.")
        MY_DEBUG=True
 
    app.run( port=MY_PORT, debug=MY_DEBUG )


