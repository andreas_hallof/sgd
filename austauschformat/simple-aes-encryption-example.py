#! /usr/bin/env python3

import os, base64, sys
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

with open("phrkey.xml","rt") as f:
    data = f.read()

data=data.encode()

aad = b"ACME Ableitungsschluessel Q1 2019"
#key = AESGCM.generate_key(bit_length=128)
key=b'abcdefghijklmnopqrstuvwxyz123456'
aesgcm = AESGCM(key)
#nonce = os.urandom(12)
nonce = b'123456789012'

# hinweis: nonce is in der nomenklaur der python-cryptography-api 
# das gleiche wie iv
ct = aesgcm.encrypt(nonce, data, aad)

print(base64.b64encode(aad))

nonce_and_ct=nonce+ct
print(base64.b64encode(nonce_and_ct))

pt = aesgcm.decrypt(nonce, ct, aad)
print(pt.decode())


