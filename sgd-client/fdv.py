#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, re, secrets, requests, json
from base64 import b64encode, b64decode
from binascii import hexlify, unhexlify
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives import serialization
sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ecies') )
from ecies import *

if __name__=='__main__':

    URL_SGD_1="http://127.0.0.1:9000/SGD1"
    URL_SGD_2="http://127.0.0.1:9000/SGD2"

    HTTP_Headers = {'content-type': 'application/json'}

    with open("../pki/egk-1-aut.der", "rb") as f:
        cert_der=f.read()
        B_cert=b64encode(cert_der).decode() 

    cert_obj=x509.load_der_x509_certificate(cert_der, default_backend())
    for x in [ y.value for y in cert_obj.subject ]:
        #print(x)
        so=re.search('([aA]\d{9})', x)
        if so:
            KVNR=so.group(0)

    assert KVNR 
    print("Meine KVNR:", KVNR)

    GetPublicKey_string=json.dumps(
        { 'Command'     : 'GetPublicKey', 
          'Certificate' : B_cert,
          'OCSPResponse': ""
        }
    )

    GetPublicKey_data=\
        { 'Command'     : 'GetPublicKey', 
          'Certificate' : B_cert,
          'OCSPResponse': ""
        }
    
    banner("Schritt 1")
    print("SGD-HSM-Schlüssel von SGD 1 erhalten (GetPublicKey)")
    #r = requests.post(URL_SGD_1, data=GetPublicKey_string, headers=HTTP_Headers)
    r = requests.post(URL_SGD_1, json=GetPublicKey_data)
    #print("Response:", r.text)
    print("Headers:" , r.headers)
    print("Content-Type:", r.headers["content-type"])
    print("HTTP-Status:", r.status_code)
    assert r.status_code == requests.codes.ok
    # das müsste dann als 'try' gemacht werden falls der server doch kein
    # json zurückliefert
    res=r.json()
    print("HHH", type(res))
    print(res)
    if type(res)!="<class 'dict'>":
        res=json.loads(res)
        print("Warning: MIME-Type stimmt evtl. immernoch nicht im Server")
    
    print("|",type(res),"|", res)

    PublicKeyECIES=res['PublicKeyECIES'].encode()
    # xxx 'Certificate' prüfen
    # xxx Signatur prüfen
    H_raw=hashes.Hash(hashes.SHA256(), backend=default_backend())
    H_raw.update(PublicKeyECIES)
    H_SGD1=hexlify(H_raw.finalize()).decode()
    print(H_SGD1)
    SGD_1_PubKey=PublicKeyECIES.decode()

    banner("Schritt 2")
    print("SGD-HSM-Schlüssel von SGD 2 erhalten (GetPublicKey)")
    r = requests.post(URL_SGD_1, data=GetPublicKey_string, headers=HTTP_Headers)
    print("HTTP-Status:", r.status_code)
    assert r.status_code == requests.codes.ok
    # das müsste dann als 'try' gemacht werden falls der server doch kein
    # json zurückliefert
    res=r.json()
    if type(res)!="<class 'dict'>":
        res=json.loads(res)
        print("Warning: MIME-Type stimmt evtl. immernoch nicht im Server")

    #print(res)
    PublicKeyECIES_String=res['PublicKeyECIES']
    # xxx 'Certificate' prüfen
    # xxx Signatur prüfen
    H_raw=hashes.Hash(hashes.SHA256(), backend=default_backend())
    H_raw.update(PublicKeyECIES)
    H_SGD2=hexlify(H_raw.finalize()).decode()
    print(H_SGD2)
    SGD_2_PubKey=PublicKeyECIES.decode()

    banner("Schritt 3")
    print("Client-ECIES-Schlüssel erzeugen")

    private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
    public_key  = private_key.public_key()
    pn          = public_key.public_numbers()
    
    public_key_string = "{} {} {} {} {}".format(pn.curve.name, hex(pn.x), hex(pn.y), H_SGD1, H_SGD2)
    #t=pn.public_key(default_backend())

    #tmp=public_key.public_bytes(Encoding.DER, PublicFormat.SubjectPublicKeyInfo)
    #print("Start", hexlify(tmp))
    #tmp2=serialization.load_der_public_key(tmp, backend=default_backend())

    print(public_key_string)

    with open("../pki/egk-1-aut-key.pem", "rb") as f:
        serialized_egk_private=f.read()
        #print(serialized_private)

    private_egk_key = serialization.load_pem_private_key(
        serialized_egk_private,
        password=None,
        backend=default_backend()
    )

    signature = private_egk_key.sign( public_key_string.encode(), ec.ECDSA(hashes.SHA256()) )
    B_signature=b64encode(signature).decode()
    print("Signatur:", B_signature)

    banner("Schritt 4")
    print("GetAuthenticationToken bei SGD1 aufrufen")
    Challenge=secrets.token_hex(32)
    message="Challenge {}".format(Challenge)
    print("Klartext ist:", message)
    ciphertext=EncryptECIES(SGD_1_PubKey, message)

    GetAuthenticationToken_string=json.dumps(
        { 'Command'             : 'GetAuthenticationToken', 
          'PublicKeyECIES'      : public_key_string,
          'Signature'           : B_signature,
          'Certificate'         : B_cert,
          'EncryptedMessage'    : ciphertext,
          'OCSPResponse'        : ""
        }
    )
    H_raw=hashes.Hash(hashes.SHA256(), backend=default_backend())
    H_raw.update(public_key_string.encode() + cert_der )
    Hashwert_GAT=hexlify(H_raw.finalize()).decode()

    print("Client-Nachricht für GetAuthenticationToken-Operation:",
          GetAuthenticationToken_string)

    r = requests.post(URL_SGD_1, data=GetAuthenticationToken_string, headers=HTTP_Headers)
    print("Response:", r.text)
    print("Headers:" , r.headers)
    print("HTTP-Status:", r.status_code)
    assert r.status_code == requests.codes.ok
    # das müsste dann als 'try' gemacht werden falls der server doch kein
    # json zurückliefert
    res=r.json()
    if type(res)!="<class 'dict'>":
        res=json.loads(res)
        print("Warning: MIME-Type stimmt evtl. immernoch nicht im Server")
    
    print("|",type(res),"|", res)
    assert res['Status'] == 'OK'
    assert "EncryptedMessage" in res

    plaintext=DecryptECIES(private_key, res['EncryptedMessage'])
    print(plaintext)
    #so=re.search('^Response ([A-Fa-f0-9]{32}) ([A-Fa-f0-9]{32}) (AT[A-Fa-f0-9]{32})', plaintext)
    so=re.search('^Response ([A-Fa-f0-9]{64}) ([A-Fa-f0-9]{64}) (AT[A-Fa-f0-9]{64})', plaintext)
    if not so:
        sys.exit("invalid response from server")

    assert Challenge==so.group(1)
    assert Hashwert_GAT==so.group(2)
    AT_SGD1=so.group(3)

    banner("Schritt 6")
    print("KeyDerivation bei SGD1 aufrufen")

    RequestID=secrets.token_hex(32)
    plaintext=AT_SGD1 + " " + RequestID + " KeyDerivation r1:" + KVNR
    print("Klartext:", plaintext)
    ciphertext=EncryptECIES(SGD_1_PubKey, plaintext)

    KeyDerivation_string=json.dumps(
        { 'Command'             : 'KeyDerivation', 
          'PublicKeyECIES'      : public_key_string,
          'Signature'           : B_signature,
          'Certificate'         : B_cert,
          'EncryptedMessage'    : ciphertext,
          'OCSPResponse'        : ""
        }
    )
    r = requests.post(URL_SGD_1, data=KeyDerivation_string, headers=HTTP_Headers)
    print("Response:", r.text)
    print("Headers:" , r.headers)
    print("HTTP-Status:", r.status_code)
    assert r.status_code == requests.codes.ok
    # das müsste dann als 'try' gemacht werden falls der server doch kein
    # json zurückliefert
    res=r.json()
    if type(res)!="<class 'dict'>":
        res=json.loads(res)
        print("Warning: MIME-Type stimmt evtl. immernoch nicht im Server")
    
    print("|",type(res),"|", res)
    assert res['Status'] == 'OK'
    assert "EncryptedMessage" in res

    plaintext=DecryptECIES(private_key, res['EncryptedMessage'])
    print(plaintext)




    #    banner("2")
    #    print("Entschlüssele Nachricht")
    #    plaintext=DecryptECIES(private_key, ciphertext)
    #    print("Plaintext:\n", plaintext)
    #
    


