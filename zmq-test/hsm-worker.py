#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import zmq, time, sys, signal

# https://stackoverflow.com/questions/1112343/how-do-i-capture-sigint-in-python
def signal_handler(sig, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


WORKER1_TCP_PORT=6123

context = zmq.Context()
socket = context.socket(zmq.REP)
# https://stackoverflow.com/questions/6024003/why-doesnt-zeromq-work-on-localhost
socket.bind("tcp://127.0.0.1:{}".format(WORKER1_TCP_PORT))

while True:
    #  Wait for next request from client
    message = socket.recv()
    print("Received request:", message)
    socket.send(b"Ret " + message)

